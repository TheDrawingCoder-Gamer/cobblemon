/*
 * Copyright (C) 2023 Cobblemon Contributors
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.cobblemon.mod.common.command

import com.cobblemon.mod.common.api.permission.CobblemonPermissions
import com.cobblemon.mod.common.command.argument.PartySlotArgumentType
import com.cobblemon.mod.common.command.argument.StatusArgumentType
import com.cobblemon.mod.common.pokemon.status.PersistentStatus
import com.cobblemon.mod.common.util.alias
import com.cobblemon.mod.common.util.commandLang
import com.cobblemon.mod.common.util.permission
import com.cobblemon.mod.common.util.player
import com.cobblemon.mod.common.api.text.*
import com.cobblemon.mod.common.util.lang
import com.cobblemon.mod.common.pokemon.status.PersistentStatusContainer
import com.mojang.brigadier.Command
import com.mojang.brigadier.CommandDispatcher
import com.mojang.brigadier.context.CommandContext
import com.mojang.brigadier.arguments.IntegerArgumentType
import net.minecraft.command.argument.EntityArgumentType
import net.minecraft.server.command.CommandManager.argument
import net.minecraft.server.command.CommandManager.literal
import net.minecraft.server.command.ServerCommandSource
import net.minecraft.server.network.ServerPlayerEntity

object PokeStatusCommand {
    private const val NAME = "pokemonstatus"
    private const val NAME_OTHER = "${NAME}other"
    private const val PLAYER = "player"
    private const val SLOT = "slot"
    private const val STATUS = "status"
    private const val TIME = "time"
    private const val ALIAS = "pokestatus"
    private const val ALIAS_OTHER = "${ALIAS}other"
    
    fun register(dispatcher : CommandDispatcher<ServerCommandSource>) {
        val selfCommand = dispatcher.register(literal(NAME)
            .permission(CobblemonPermissions.POKEMON_STATUS_SELF)
            .then(argument(SLOT, PartySlotArgumentType.partySlot())
                .then(argument(STATUS, StatusArgumentType.status())
                    .then(argument(TIME, IntegerArgumentType.integer(0))
                        .executes { execute(it, it.source.playerOrThrow) }
                    )
                )
            )
        )
        
        dispatcher.register(selfCommand.alias(ALIAS))

        val otherCommand = dispatcher.register(literal(NAME_OTHER)
            .permission(CobblemonPermissions.POKEMON_STATUS_OTHER)
            .then(argument(PLAYER, EntityArgumentType.player())
                .then(argument(SLOT, PartySlotArgumentType.partySlot())
                    .then(argument(STATUS, StatusArgumentType.status())
                        .then(argument(TIME, IntegerArgumentType.integer(0))
                                .executes { execute(it, it.player()) }
                        )
                    )
                )
            )
        )
    

        dispatcher.register(otherCommand.alias(ALIAS_OTHER))

            
    }

    fun execute(context: CommandContext<ServerCommandSource>, player: ServerPlayerEntity): Int {
        val pokemon = PartySlotArgumentType.getPokemonOf(context, SLOT, player)
        val oldName = pokemon.species.translatedName
        val status = StatusArgumentType.getStatus(context, STATUS)
        val time = IntegerArgumentType.getInteger(context, TIME)
        if (status !is PersistentStatus) {
            player.sendMessage(lang("command.pokemonstatus.notpersistent").red())
            return 0
        }
        pokemon.status = PersistentStatusContainer(status, time)
        
        context.source.sendFeedback(commandLang(NAME, oldName, player.name, status.showdownName), true)
        return Command.SINGLE_SUCCESS

    }
}
