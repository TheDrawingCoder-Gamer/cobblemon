/*
 * Copyright (C) 2023 Cobblemon Contributors
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.cobblemon.mod.common.command.argument

import com.cobblemon.mod.common.api.pokemon.status.Statuses
import com.cobblemon.mod.common.api.pokemon.status.Status
import com.cobblemon.mod.common.Cobblemon
import com.cobblemon.mod.common.api.text.red
import com.cobblemon.mod.common.pokemon.Pokemon
import com.cobblemon.mod.common.util.commandLang
import com.mojang.brigadier.StringReader
import com.mojang.brigadier.arguments.ArgumentType
import com.mojang.brigadier.context.CommandContext
import com.mojang.brigadier.exceptions.CommandSyntaxException
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType
import com.mojang.brigadier.suggestion.Suggestions
import com.mojang.brigadier.suggestion.SuggestionsBuilder
import java.util.concurrent.CompletableFuture
import net.minecraft.command.CommandSource
import com.cobblemon.mod.common.util.lang

class StatusArgumentType : ArgumentType<Status> {
    companion object {
        val EXAMPLES: List<String> = listOf(Statuses.BURN.showdownName)
        val INVALID_STATUS = lang("command.pokemonstatus.invalid-status")

        fun status() = StatusArgumentType()

        fun <S> getStatus(context: CommandContext<S>, name: String): Status {
            return context.getArgument(name, Status::class.java)
        }
    }

    override fun parse(reader: StringReader): Status {
        val name = reader.readString()
        return Statuses.getStatus(name) ?: throw SimpleCommandExceptionType(INVALID_STATUS).createWithContext(reader)
    }

    override fun <S: Any> listSuggestions(
        context: CommandContext<S>,
        builder: SuggestionsBuilder
    ): CompletableFuture<Suggestions> {
        return CommandSource.suggestMatching(Statuses.all().map { it.showdownName }, builder)
    }

    override fun getExamples() = EXAMPLES
}
